package com.sample.foo.samplecalculator;

import android.content.Context;
import android.support.test.InstrumentationRegistry;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.widget.TextView;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Arrays;
import java.util.HashSet;

import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.junit.Assert.*;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class MainActivityInstrumentedTest {
    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);
    
    
    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();
        
        assertEquals("com.sample.foo.samplecalculator", appContext.getPackageName());
    }
    
    
    @Test
    public void testAddition() {
        onView(withId(R.id.buttonTwo)).perform(click());
        onView(withId(R.id.buttonAdd)).perform(click());
        onView(withId(R.id.buttonTwo)).perform(click());
        onView(withId(R.id.buttonEqual)).perform(click());
        
        onView(withId(R.id.infoTextView)).check(matches(withText("2+2 = 4")));

//        TextView infoTextView = mActivityRule.getActivity().findViewById(R.id.infoTextView);
//        assertEquals(infoTextView.getText().toString(), "2+2 = 4");
        
    }
    
    
    // fails, because of unsupported unary minus
    @Test
    public void enterNegativeNumbers() {
        clickAll("-2+2=");
        onView(withId(R.id.infoTextView)).check(matches(withText("-2+2 = 0")));
    }
    
    
    // crashes the app
    @Test
    public void enterEqualityAfterIncompleteExpr() {
        clickAll("2*=");
        onView(withId(R.id.infoTextView)).check(matches(withText("не число")));
    }
    
    
    // crashes the app
    @Test
    public void enterDoubleOperation() {
        clickAll("2*-2=");
        onView(withId(R.id.infoTextView)).check(matches(withText("2*-2 = -4")));
    }
    
    
    
    private void clickOne(char sign) {
        int id;
        switch (sign) {
            case '+': id = R.id.buttonAdd; break;
            case '-': id = R.id.buttonSubtract; break;
            case '*': id = R.id.buttonMultiply; break;
            case '/': id = R.id.buttonDivide; break;
            case '0': id = R.id.buttonZero; break;
            case '1': id = R.id.buttonOne; break;
            case '2': id = R.id.buttonTwo; break;
            case '3': id = R.id.buttonThree; break;
            case '4': id = R.id.buttonFour; break;
            case '5': id = R.id.buttonFive; break;
            case '6': id = R.id.buttonSix; break;
            case '7': id = R.id.buttonSeven; break;
            case '8': id = R.id.buttonEight; break;
            case '9': id = R.id.buttonNine; break;
            case '.': id = R.id.buttonDot; break;
            case '=': id = R.id.buttonEqual; break;
            case 'c': id = R.id.buttonClear; break;
//            default: throw new IllegalArgumentException("sign " + sign + " is invalid");
            default: return; // just ignore unknown symbol (like space)
        }
        onView(withId(id)).perform(click());
    }
    
    private void clickAll(String symbols) {
        for (char symbol : symbols.toCharArray()) {
            clickOne(symbol);
        }
    }
    
    /**
     * For parametrized test.
     * Precision is four digits after zero.
     * Only the first operand can be negative, because we can replace it with 0-n combination.
     * 
     * num1 and num2, result are either floats or integers
     */
    void performSimpleOperation(Number num1, char sign, Number num2, Number result) {
        if (num2.doubleValue() < 0 || !Arrays.asList('+', '-', '*', '/').contains(sign))
            throw new IllegalArgumentException("illegal argument num2=" + num2 + " or sign=" + sign);
        
        if (num1.doubleValue() < 0) {
            clickOne('0');
        }
        
        // fits for input and output checks
        String expressionLeft = "" + num1 + sign + num2 + " = ";
        clickAll(expressionLeft);
        
        String resultStr;
        if (Double.isNaN(result.doubleValue())) {
            // translation depends on device language!!!!
//            resultStr = "не число";
            resultStr = "NaN";
        } else if (result.doubleValue() == Double.POSITIVE_INFINITY) {
            resultStr = "∞";
        } else if (result.doubleValue() == Double.NEGATIVE_INFINITY) {
            resultStr = "-∞";
        } else {
            resultStr = "" + result;
        }
        
        // do not use it, because of bad error messages
//        onView(withId(R.id.infoTextView)).check(matches(withText(expressionLeft + resultStr)));
        
        TextView infoTextView = mActivityRule.getActivity().findViewById(R.id.infoTextView);
        // translation depends on device language
        String infoText = infoTextView.getText().toString().replace("не число", "NaN");
        assertEquals(infoText, expressionLeft + resultStr);
        
        // clear
        clickAll("ccc");
    }
    
    
    
    
    @Test
    public void adds() {
        performSimpleOperation(2, '+', 2, 4);
        performSimpleOperation(2, '+', 2.5, 4.5);
        performSimpleOperation(-2, '+', 2.5, 0.5);
        performSimpleOperation(-2.5, '+', 2.5, 0);
        performSimpleOperation(-2.5, '+', 0, -2.5);
    }
    
    
    @Test
    public void subs() {
        performSimpleOperation(2, '-', 2, 0);
        performSimpleOperation(2, '-', 2.5, -0.5);
        performSimpleOperation(2, '-', 0, 2);
        performSimpleOperation(-2, '-', 2, -4);
    }
    
    @Test
    public void mults() {
        performSimpleOperation(2, '*', 3, 6);
        performSimpleOperation(-2, '*', 2.5, -5);
    }
    
    @Test
    public void divs() {
        performSimpleOperation(2, '/', 2, 1);
        performSimpleOperation(-4, '/', 2, -2);
        // not supported
        // performSimpleOperation(-4, '/', -2, 2);
        performSimpleOperation(0, '/', 10, 0);
    
        performSimpleOperation(0, '/', 0, Double.NaN);
        performSimpleOperation(25, '/', 0, Double.POSITIVE_INFINITY);
        performSimpleOperation(-25, '/', 0, Double.NEGATIVE_INFINITY);
        
        
        // don't check borders, because there aren't any requirements about them.
    }
    
    
    
    
    
}
